package gsalaun1.postgresqlcustomtype

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PostgresqlCustomTypeApplication

fun main(args: Array<String>) {
    runApplication<PostgresqlCustomTypeApplication>(*args)
}
