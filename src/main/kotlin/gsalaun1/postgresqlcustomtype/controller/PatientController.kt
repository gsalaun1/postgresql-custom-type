package gsalaun1.postgresqlcustomtype.controller

import gsalaun1.postgresqlcustomtype.service.PatientService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.util.UUID

@Controller
@RequestMapping("/patient")
class PatientController(
    val patientService: PatientService
) {

    @PostMapping
    fun save(
        @ModelAttribute("patient") patientRequest: PatientRequest
    ): String {
        patientRequest.doctor?.let {
            patientService.create(patientRequest.name, it)
        }
        return "redirect:/"
    }

    @GetMapping("/delete/{uid}")
    fun delete(@PathVariable uid: UUID): String {
        patientService.delete(uid)
        return "redirect:/"
    }
}
