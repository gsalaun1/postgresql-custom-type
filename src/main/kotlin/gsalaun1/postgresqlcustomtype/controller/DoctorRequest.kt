package gsalaun1.postgresqlcustomtype.controller

data class DoctorRequest(
    val name: String
)
