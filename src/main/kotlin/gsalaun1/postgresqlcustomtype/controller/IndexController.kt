package gsalaun1.postgresqlcustomtype.controller

import gsalaun1.postgresqlcustomtype.service.DoctorService
import gsalaun1.postgresqlcustomtype.service.PatientService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class IndexController(
    val doctorService: DoctorService,
    val patientService: PatientService
) {

    @GetMapping("/")
    fun index(model: Model): String {
        model.addAttribute("doctor", DoctorRequest(""))
        model.addAttribute("doctors", doctorService.findAll())
        model.addAttribute("patient", PatientRequest(""))
        model.addAttribute("patients", patientService.findAll())
        return "index"
    }
}
