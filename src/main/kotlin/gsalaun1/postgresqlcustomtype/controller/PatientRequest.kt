package gsalaun1.postgresqlcustomtype.controller

import java.util.UUID

class PatientRequest(
    val name: String,
    val doctor: UUID? = null
)
