package gsalaun1.postgresqlcustomtype.controller

import gsalaun1.postgresqlcustomtype.service.DoctorService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.util.UUID

@Controller
@RequestMapping("/doctor")
class DoctorController(
    val doctorService: DoctorService
) {

    @PostMapping
    fun save(
        @ModelAttribute("doctor") doctorRequest: DoctorRequest
    ): String {
        doctorService.create(doctorRequest.name)
        return "redirect:/"
    }

    @GetMapping("/delete/{uid}")
    fun delete(@PathVariable uid: UUID): String {
        doctorService.delete(uid)
        return "redirect:/"
    }
}
