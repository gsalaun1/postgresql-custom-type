package gsalaun1.postgresqlcustomtype.service

import gsalaun1.postgresqlcustomtype.repository.DoctorEntity
import gsalaun1.postgresqlcustomtype.repository.DoctorRepository
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class DoctorService(
    val doctorRepository: DoctorRepository
) {

    fun create(name: String) {
        doctorRepository.save(DoctorEntity(name = name))
    }

    fun delete(uid: UUID) {
        doctorRepository.deleteById(uid)
    }

    fun findAll() = doctorRepository.findAll().map { it.toDomain() }.toSortedSet()
}
