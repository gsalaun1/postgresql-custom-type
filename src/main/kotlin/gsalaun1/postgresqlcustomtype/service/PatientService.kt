package gsalaun1.postgresqlcustomtype.service

import gsalaun1.postgresqlcustomtype.repository.DoctorRepository
import gsalaun1.postgresqlcustomtype.repository.PatientEntity
import gsalaun1.postgresqlcustomtype.repository.PatientRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class PatientService(
    val doctorRepository: DoctorRepository,
    val patientRepository: PatientRepository
) {
    fun create(name: String, doctorUid: UUID) {
        doctorRepository.findByIdOrNull(doctorUid)?.let {
            val patient = PatientEntity(name = name, doctor = it)
            patientRepository.save(patient)
        }
    }

    fun delete(uid: UUID) {
        patientRepository.deleteById(uid)
    }

    fun findAll() = patientRepository.findAll().map { it.toDomain() }.toSortedSet()
}
