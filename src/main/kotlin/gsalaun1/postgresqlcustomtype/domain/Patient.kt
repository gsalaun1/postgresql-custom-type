package gsalaun1.postgresqlcustomtype.domain

import java.util.UUID

data class Patient(
    val uid: UUID,
    val name: String,
    val doctor: Doctor
) : Comparable<Patient> {
    override fun compareTo(other: Patient) =
        compareValuesBy(this, other, { it.name }, { it.uid })
}
