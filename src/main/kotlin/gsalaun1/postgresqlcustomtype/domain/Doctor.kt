package gsalaun1.postgresqlcustomtype.domain

import java.util.SortedSet
import java.util.UUID

data class Doctor(
    val uid: UUID,
    val name: String,
    val patients: SortedSet<Patient>
) : Comparable<Doctor> {
    override fun compareTo(other: Doctor) =
        compareValuesBy(this, other, { it.name }, { it.uid })
}
