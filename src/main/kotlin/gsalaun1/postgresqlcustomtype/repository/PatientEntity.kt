package gsalaun1.postgresqlcustomtype.repository

import gsalaun1.postgresqlcustomtype.domain.Doctor
import gsalaun1.postgresqlcustomtype.domain.Patient
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "patient")
class PatientEntity(
    @Id val uid: UUID = UUID.randomUUID(),

    val name: String,

    @ManyToOne @JoinColumn(name = "doctor_uid")
    val doctor: DoctorEntity
) {
    fun toDomain() = Patient(uid, name, Doctor(doctor.uid, doctor.name, emptySet<Patient>().toSortedSet()))
}
