package gsalaun1.postgresqlcustomtype.repository

import gsalaun1.postgresqlcustomtype.domain.Doctor
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "doctor")
class DoctorEntity(
    @Id val uid: UUID = UUID.randomUUID(),

    val name: String,

    @OneToMany(
        cascade = [CascadeType.REMOVE],
        mappedBy = "doctor"
    )
    val patients: Set<PatientEntity> = emptySet()
) {
    fun toDomain(): Doctor = Doctor(uid, name, patients.map { it.toDomain() }.toSortedSet())
}
