package gsalaun1.postgresqlcustomtype.repository

import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface PatientRepository : JpaRepository<PatientEntity, UUID>
