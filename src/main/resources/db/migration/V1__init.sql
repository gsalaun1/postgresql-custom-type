CREATE TABLE DOCTOR
(
    uid  uuid PRIMARY KEY,
    name VARCHAR not null
);
CREATE TABLE PATIENT
(
    uid        uuid PRIMARY KEY,
    name       VARCHAR not null,
    doctor_uid uuid,
    CONSTRAINT fk_patient_doctor_uid_doctor_uid
        FOREIGN KEY (doctor_uid)
            REFERENCES DOCTOR (uid)
);

INSERT INTO DOCTOR
VALUES ('5f1fa980-2c3d-43ee-b8cb-0558ae5d4905', 'Doctor 1'),
       ('c1e45990-0aef-47f2-a2ad-8b65724aa238', 'Doctor 2'),
       ('f91188aa-c5e7-42e8-bad7-9696461a3d21', 'Doctor 3');

INSERT INTO PATIENT
VALUES ('cf57447a-e216-4f90-89b0-9e11a2d4701c', 'Patient 1', '5f1fa980-2c3d-43ee-b8cb-0558ae5d4905'),
       ('5a38909c-86ec-428b-b0ae-1e2d72dee900', 'Patient 2', '5f1fa980-2c3d-43ee-b8cb-0558ae5d4905'),
       ('5283e50b-8a51-4dd5-b97f-4574e08e6cb8', 'Patient 3', '5f1fa980-2c3d-43ee-b8cb-0558ae5d4905'),
       ('da2e1006-ef9c-48cd-a12c-67672790dc85', 'Patient 4', '5f1fa980-2c3d-43ee-b8cb-0558ae5d4905'),
       ('c3fea20f-0cb1-4526-9b11-eb77f0532089', 'Patient 5', 'c1e45990-0aef-47f2-a2ad-8b65724aa238'),
       ('e1a8a66f-5542-450e-bf6a-7cc6f67121ca', 'Patient 6', 'c1e45990-0aef-47f2-a2ad-8b65724aa238');