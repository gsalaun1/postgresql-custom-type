package gsalaun1.postgresqlcustomtype.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.repository.findByIdOrNull
import java.util.UUID

@DataJpaTest
class DoctorRepositoryTest {

    @Autowired
    lateinit var doctorRepository: DoctorRepository

    @Test
    fun `Should return all doctors`() {
        val doctors = doctorRepository.findAll()
        assertThat(doctors).hasSize(3)
    }

    @Test
    fun `Should find a doctor`() {
        val doctor = doctorRepository.findByIdOrNull(UUID.fromString("5f1fa980-2c3d-43ee-b8cb-0558ae5d4905"))
        assertThat(doctor).isNotNull
    }

    @Test
    fun `Should save a doctor`() {
        val name = "New Doctor"
        val doctor = doctorRepository.save(DoctorEntity(name = name))
        val doctors = doctorRepository.findAll()
        assertThat(doctors.filter { it.name == name }.first().uid).isEqualTo(doctor.uid)
    }
}
